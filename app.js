"use strict";

var express = require('express');
var fs = require('fs');
var app = express();
var bodyParser = require('body-parser')

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.use( bodyParser.json() );       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
}));

//app.use(express.multipart());


function escapeRegexCharacters(str) {
  return str.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
}

function getSuggestions(value, data) {
  const escapedValue = escapeRegexCharacters(value.trim());

  if (escapedValue === '') {
    return [];
  }

  const regex = new RegExp('^' + escapedValue, 'i');

  return data.filter(language => regex.test(language.name));
}

app.get('/languages', function(req, res) {
  var result;
  let path = "./data/formatSearch.json";
  let data = JSON.parse(fs.readFileSync(path, 'utf8'));

  result = data;
  // if (req.query.filter !== undefined) {
  //   result = getSuggestions(req.query.filter, data);
  // }
  // else {
  //   result = data;
  // }

  res.send(result);
});

app.get('/assets', function(req, res) {
  var result;
  console.log("test for get"+req.query.productname);
  let path = "./data/assets.json";
  let data = JSON.parse(fs.readFileSync(path, 'utf8'));

  result = data;
  res.send(result);
});

app.get('/assetsData', function(req, res) {
  var result;
  console.log("test for get"+req.query.productname);
  let path = "./data/assetsData.json";
  let data = JSON.parse(fs.readFileSync(path, 'utf8'));

  result = data;
  res.send(result);
});

app.get('/assetsDataSelectBox', function(req, res) {
  var result;
  console.log("test for get");
  let path = "./data/assetsDataSelectBox.json";
  let data = JSON.parse(fs.readFileSync(path, 'utf8'));

  result = data;
  res.send(result);
});

app.get('/saveAssetsData', function(req, res) {
  var result;
  let path = "./data/saveAssetsData.json";
  let data = JSON.parse(fs.readFileSync(path, 'utf8'));

  result = data;
  res.send(result);
});


app.get('/metaData', function(req, res) {
  var result;
  let path = "./data/metaData.json";
  let data = JSON.parse(fs.readFileSync(path, 'utf8'));

  result = data;
  res.send(result);
});

app.get('/saveMetaData', function(req, res) {
  var result;
  let path = "./data/saveMetaData.json";
  let data = JSON.parse(fs.readFileSync(path, 'utf8'));

  result = data;
  res.send(result);
});

app.post('/saveAssetsDataPost', function(req, res) {
  //console.log("test"+req.body.productName);
  //console.log("test"+req.body.author);
  console.log(req.body);
  var result;
  let path = "./data/saveAssetsData.json";
  let data = JSON.parse(fs.readFileSync(path, 'ut f8'));

  result = data;
  res.send(result);
});

app.get('/autoCompleteData', function(req, res) {
  console.log("test"+req.query.text)
  var result;
  let path = "./data/autoCompleteData.json";
  let data = JSON.parse(fs.readFileSync(path, 'utf8'));

  result = data;
  res.send(result);
});

app.get('/questionMetaData', function(req, res) {
  console.log("test"+req.query.text)
  var result;
  let path = "./data/questionMetaData.json";
  let data = JSON.parse(fs.readFileSync(path, 'utf8'));

  result = data;
  res.send(result);
});

app.get('/saveQuestionMetaData', function(req, res) {
  var result;
  let path = "./data/saveQuestionMetaData.json";
  let data = JSON.parse(fs.readFileSync(path, 'utf8'));

  result = data;
  res.send(result);
});

app.get('/autoCompleteDataSugg', function(req, res) {
  var result;
  let path = "./data/autoCompleteDataSugg.json";
  let data = JSON.parse(fs.readFileSync(path, 'utf8'));

  result = data;
  res.send(result);
});

app.get('/singleUploadData', function(req, res) {
  var result;
  let path = "./data/singleUploadData.json";
  let data = JSON.parse(fs.readFileSync(path, 'utf8'));

  result = data;
  res.send(result);
});

app.get('/fileUpload', function(req, res) {
  var result;
  let path = "./data/fileUpload.json";
  let data = JSON.parse(fs.readFileSync(path, 'utf8'));

  result = data;
   setInterval(function() {
        res.send(result);
    },20000);
});

app.get('/assessmentMetaData', function(req, res) {
  console.log("test"+req.query.text)
  var result;
  let path = "./data/assessmentMetaData.json";
  let data = JSON.parse(fs.readFileSync(path, 'utf8'));

  result = data;
  res.send(result);
});


app.listen(3000, function() {
  console.log('Example app listening on port 3000!');
});